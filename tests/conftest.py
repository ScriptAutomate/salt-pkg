import logging
import os
import pathlib
import pprint
import re
import shutil
import tarfile
import time

import distro
import pytest

import tests.support.helpers


log = logging.getLogger(__name__)


@pytest.fixture(scope="session")
def version():
    """
    get version number from artifact
    """
    _version = ""
    for artifact in tests.support.helpers.ARTIFACTS_DIR.glob("**/*.*"):
        _version = re.search(
            r"([0-9].*)(\-[0-9].el|\+ds|\-[0-9].am|\-[0-9]-[a-z]*-[a-z]*[0-9]*.tar.gz)",
            artifact.name,
        )
        if _version:
            _version = _version.groups()[0].replace("_", "-").replace("~", "")
            break
    return _version


def install_package(pkg_mngr, pkgs):
    # install packages
    pkg_install = [pkg_mngr, "install", "-y"]
    log.debug("Installing packages:\n%s", pprint.pformat(pkgs))
    ret = tests.support.helpers.run(pkg_install + pkgs)
    assert ret["retcode"] == 0


def uninstall_package(pkg_mngr, pkgs, rm_pkg, salt_pkgs):
    # remove packages
    pkg_remove = [pkg_mngr, rm_pkg, "-y"]
    log.debug("Un-Installing packages:\n%s", pprint.pformat(pkgs))
    ret = tests.support.helpers.run(pkg_remove + salt_pkgs)
    assert ret["retcode"] == 0


def install_singlebin(pkgs):
    pkg = pkgs[0]
    tar = tarfile.open(pkg)  # , "r:gz")
    tar.extractall(path=pathlib.Path(os.sep) / "usr" / "local" / "bin")
    tar.close()


def uninstall_singlebin():
    pathlib.Path.unlink(pathlib.Path(os.sep) / "usr" / "local" / "bin" / "salt")


@pytest.fixture(autouse=True, scope="package")
def install_salt():
    """
    Install Salt
    """
    # todo make the pkg path configurable
    distro_id = distro.id().lower()
    salt_pkgs = [
        "salt-api",
        "salt-syndic",
        "salt-ssh",
        "salt-master",
        "salt-cloud",
        "salt-minion",
    ]
    salt_fix = tests.support.helpers.Pkg()
    pkgs = salt_fix.pkgs
    singlebin = salt_fix.singlebin

    pkg_mngr = None
    rm_pkg = None
    if distro_id in ["centos", "redhat", "amzn"]:
        salt_pkgs.append("salt")
        pkg_mngr = "yum"
        rm_pkg = "remove"

    elif distro_id in ["ubuntu", "debian"]:
        salt_pkgs.append("salt-common")
        pkg_mngr = "apt-get"
        rm_pkg = "purge"
        tests.support.helpers.run(["apt-get", "update"])

    if singlebin:
        install_singlebin(pkgs=pkgs)
        yield
        uninstall_singlebin()
    else:
        install_package(pkg_mngr, pkgs=pkgs)
        yield
        uninstall_package(pkg_mngr, pkgs=pkgs, rm_pkg=rm_pkg, salt_pkgs=salt_pkgs)


@pytest.fixture(scope="module")
def sls():
    """
    Add an sls file
    """
    file_root = pathlib.Path("/srv/salt")
    file_root.mkdir(mode=0o777, parents=True, exist_ok=True)
    test_sls = file_root / "test.sls"
    state_sls = file_root / "states.sls"
    win_state_sls = file_root / "win_states.sls"
    with open(test_sls, "w") as _fh:
        _fh.write(
            """
            test_foo:
              test.succeed_with_changes:
                  - name: foo
        """
        )
    with open(state_sls, "w") as _fh:
        _fh.write(
            """
            update:
              pkg.installed:
                - name: bash
            salt_dude:
              user.present:
                - name: dude
                - fullname: Salt Dude
        """
        )
    with open(win_state_sls, "w") as _fh:
        _fh.write(
            """
            create_empty_file:
              file.managed:
                - name: C://salt/test/txt
            salt_dude:
              user.present:
                - name: dude
                - fullname: Salt Dude
        """
        )
    try:
        yield test_sls, state_sls, win_state_sls
    finally:
        shutil.rmtree(file_root.parent)


# todo: allow someone to customize config before startup of master
@pytest.fixture(scope="module")
def master_config(contents=None):
    """
    Add the Salt Master config
    """

    def _config(contents):
        tests.support.helpers.write_config("master", contents=contents)

    return _config


# todo: allow someone to customize config before startup of minion
@pytest.fixture(scope="module")
def minion_config():
    """
    Add the Salt Minion config
    """
    contents = {"master": "127.0.0.1", "id": "pkg_tests"}
    tests.support.helpers.write_config("minion", contents=contents)

    def _config(id="pkg_tests", master="127.0.0.1", contents=None):
        """
        adding custom content to config
        """
        if not contents:
            contents = {}
        default = {"master": master, "id": id}
        contents.update(default)
        tests.support.helpers.write_config("minion", contents=contents)

    return _config


@pytest.fixture(scope="module")
def api_config():
    """
    Add the Salt Api config
    """
    contents = {
        "rest_cherrypy": {"port": 8000, "disable_ssl": True},
        "external_auth": {"auto": {"saltdev": [".*"]}},
    }
    tests.support.helpers.write_config(
        pathlib.Path("master.d/api.conf"), contents=contents
    )


@pytest.fixture(scope="module")
def start_master(master_config, salt_fixt):
    """
    Start up a master
    """
    try:
        tests.support.helpers.start_service(
            "salt-master",
            binary=salt_fixt.salt_bin["master"],
            singlebin=salt_fixt.singlebin,
        )
        yield
    finally:
        tests.support.helpers.stop_service("salt-master", singlebin=salt_fixt.singlebin)


@pytest.fixture(scope="module")
def start_minion(minion_config, salt_fixt):
    """
    Start up a minion
    """
    try:
        tests.support.helpers.start_service(
            "salt-minion",
            binary=salt_fixt.salt_bin["minion"],
            singlebin=salt_fixt.singlebin,
        )
        yield
    finally:
        tests.support.helpers.stop_service("salt-minion", singlebin=salt_fixt.singlebin)


@pytest.fixture(scope="module")
def master_minion(start_master, start_minion, salt_fixt):
    """
    Start both a master and a minion and accept keys
    """
    # accept the key after services started
    try:
        accept_key = tests.support.helpers.run(
            salt_fixt.salt_bin["key"] + ["-a", "pkg_tests", "-y"]
        )
        assert accept_key["retcode"] == 0
        time.sleep(15)
        yield
    finally:
        delete_key = tests.support.helpers.run(
            salt_fixt.salt_bin["key"] + ["-d", "pkg_tests", "-y"]
        )
        assert delete_key["retcode"] == 0


@pytest.fixture(scope="module")
def test_account():
    api_usr = tests.support.helpers.TestUser()
    try:
        api_usr.add_user()
        # setup salt-api user
        yield api_usr
    finally:
        api_usr.remove_user()


@pytest.fixture(scope="module")
def salt_api(api_config, master_minion, test_account, salt_fixt):
    """
    start up and configure salt_api
    """
    try:
        tests.support.helpers.start_service(
            "salt-api", binary=salt_fixt.salt_bin["api"], singlebin=salt_fixt.singlebin
        )

        yield test_account
    finally:
        tests.support.helpers.stop_service("salt-api", singlebin=salt_fixt.singlebin)


@pytest.fixture(scope="module")
def salt_fixt():
    return tests.support.helpers.Pkg()


@pytest.fixture(scope="module")
def salt_pillar():
    """
    Add pillar files
    """
    pillar_root = pathlib.Path("/srv/pillar")
    pillar_root.mkdir(mode=0o777, parents=True, exist_ok=True)
    top_sls = pillar_root / "top.sls"
    test_sls = pillar_root / "test.sls"

    with open(top_sls, "w") as _fh:
        _fh.write(
            """
            base:
              '*':
                - test
            """
        )

    with open(test_sls, "w") as _fh:
        _fh.write(
            """
            info: test
            """
        )
    try:
        yield top_sls, test_sls
    finally:
        shutil.rmtree(pillar_root.parent)
