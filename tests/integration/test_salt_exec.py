from sys import platform


def test_salt_cmd_run(master_minion, salt_fixt):
    """
    Test salt cmd.run 'ipconfig' or 'ls -lah /'
    """
    if platform.startswith("win"):
        ret = salt_fixt.salt_minion(["cmd.run", "ipconfig"])
    elif platform.startswith("linux"):
        ret = salt_fixt.salt_minion(["cmd.run", "ls -lah /"])
    assert ret["stdout"]


def test_salt_list_users(master_minion, salt_fixt):
    """
    Test salt user.list_users
    """
    ret = salt_fixt.salt_minion(["user.list_users"])
    if platform.startswith("win"):
        assert "Administrator" in ret["stdout"]
    elif platform.startswith("lin"):
        assert "root" in ret["stdout"]
