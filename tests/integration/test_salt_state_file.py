from sys import platform


def test_salt_state_file(sls, master_minion, salt_fixt):
    """
    Test state file
    """
    if platform == "windows":
        ret = salt_fixt.salt_minion(["state.apply", "win_states"])
    elif platform == "linux":
        ret = salt_fixt.salt_minion(["state.apply", "states"])

    sls_ret = ret["stdout"][next(iter(ret["stdout"]))]
    assert "changes" and "name" in sls_ret
