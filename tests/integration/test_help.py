import tests.support.helpers


def test_help(salt_fixt):
    """
    Test --help works for all salt cmds
    """
    for cmd in salt_fixt.salt_bin.values():
        ret = tests.support.helpers.run(cmd + ["--help"])
        assert "Usage" in ret["stdout"]
        assert ret["retcode"] == 0
