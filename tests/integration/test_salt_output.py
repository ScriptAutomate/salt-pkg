import pytest


@pytest.mark.parametrize(
    "test_input",
    [(["--output=yaml", "test.fib", "7"]), (["--output=json", "test.fib", "7"])],
)
def test_salt_output(master_minion, salt_fixt, test_input):
    """
    Test --output
    """
    ret = salt_fixt.salt_minion(test_input)
    assert 13 in ret["stdout"]
