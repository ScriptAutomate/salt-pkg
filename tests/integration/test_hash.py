import pytest

import tests.support.helpers


def test_hashes(version, salt_fixt):
    """
    Test version outputed from salt --version
    """
    if not salt_fixt.singlebin:
        pytest.skip("This test requires the single binary package")
    hashes = salt_fixt.salt_hashes
    for _hash in hashes.keys():
        ret = tests.support.helpers.run(
            ["openssl", "dgst", hashes[_hash]["tool"], salt_fixt.pkgs[0]]
        )

        with open(hashes[_hash]["file"]) as fp:
            file_hash = fp.read()
        assert ret["stdout"].split()[-1] == file_hash.split()[0]
