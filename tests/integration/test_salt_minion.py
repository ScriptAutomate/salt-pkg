def test_salt_minion_ping(master_minion, salt_fixt):
    """
    Test running a command against a targeted minion
    """
    ret = salt_fixt.salt_minion(["test.ping"])
    assert ret["stdout"]
